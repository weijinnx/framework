const api = require('../client');

module.exports = async (uri, data = {}) => {
	try {
		const res = await api.post(uri, data);
		return res.data;
	} catch (e) {
		throw e;
	}
};
