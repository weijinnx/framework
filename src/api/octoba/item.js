const api = require('../client');

module.exports = async (uri, pk, params = {}) => {
	try {
		const res = await api.get(`${uri  }/${  pk}`, {
			params: params
		});

		return res.data;
	} catch (e) {
		throw e;
	}
};
