let axios = require("axios");

const params = {
  baseURL: process.env.APP_API_URL,
  timeout: 5000
};

const instance = axios.create(params);

// Add a request interceptor
instance.interceptors.request.use(
  function(config) {
    // Do something before request is sent
    return config;
  },
  function(error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
instance.interceptors.response.use(
  function(response) {
    if (response.status !== 200) {
      console.log(response.data);
    }
    // Do something with response data
    return response;
  },
  function(error) {
    // Do something with response error
    return Promise.reject(error);
  }
);

module.exports = instance;
