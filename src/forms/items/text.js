module.exports = ctx => {
	const answer = ctx.message.text;
	const state = ctx.scene.state;
	const validators = state.form.validators;
	const errorMessage = ctx.validate(state, validators, answer, 'text', ctx);

	if (errorMessage === undefined || errorMessage === null) {
		state.data[state.form.attribute] = answer;
		ctx.wizard.next();
		return ctx.wizard.steps[ctx.wizard.cursor](ctx);
	} else {
		return ctx.reply(errorMessage);
	}
};
