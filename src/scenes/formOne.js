const Extra = require('telegraf/extra');
const WizardScene = require('telegraf/scenes/wizard');
const { getWizardFormOneSteps } = require('../utils/helpers');

/**
 * @param ctx Telegraf context
 * @param prepare Show prepare form text?
 * @param attributes Attributes in form
 * @param labels Labels for the attributes
 * @param type Type of form (store, update, updateOne)
 * @param ?id Id of item
 * @param ?uri URI
 * @param ?pk_name Name of primary key
 */
module.exports = new WizardScene('octoba-form-one', async ctx => {
	let state = ctx.scene.state;

	if (state.form.prepare) {
		await ctx.reply(ctx.i18n.t('octoba_forms_prepare'));
	}

	let formSteps = await getWizardFormOneSteps(ctx, ctx.scene.state.form.labels);

	if (Array.isArray(formSteps) && formSteps.length > 0) {
		formSteps.map(step => {
			ctx.wizard.steps.push(step);
		});

		// Last step with saving data
		ctx.wizard.steps.push(async ctx => {
			const state = ctx.scene.state.form;
			const data = ctx.scene.state.data;
			let keyboard = [];
			let pk = state.id ? state.id : null;

			ctx.reply(ctx.i18n.t('octoba_forms_saving'));

			try {
				switch (state.type) {
				case 'store':
					let res = await ctx.api.octoba.storeItem(state.uri, state.id, data);
					pk = res[state.pk_name];
					break;
				case 'update':
					await ctx.api.octoba.updateItem(state.uri, state.id, data);
					break;
				case 'updateOne':
					await ctx.api.octoba.updateOne(state.uri, state.id, data);
					break;
				default:
					ctx.reply(ctx.i18n.t('octoba_messages_unknown_type'));
					break;
				}

				let gotoKeyboard = ctx.getKeyboard('resourceGoto', {
					modelName: state.modelName,
					pk: pk,
					excluded: []
				});

				gotoKeyboard.map(kb => {
					keyboard.push(kb);
				});

				ctx.replyWithMarkdown(
					ctx.i18n.t('octoba_forms_saved'),
					Extra.markdown().markup(m => {
						return m.inlineKeyboard(keyboard);
					})
				);

				return ctx.scene.leave();
			} catch (e) {
				console.log('Error: ', e);
				ctx.reply(ctx.i18n.t('octoba_forms_saving_error'));
				ctx.wizard.back();
				return ctx.wizard.steps[ctx.wizard.cursor](ctx);
			}
		});
	}

	ctx.wizard.next();
	return ctx.wizard.steps[ctx.wizard.cursor](ctx);
});
