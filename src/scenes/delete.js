const Extra = require('telegraf/extra')
const Markup = require('telegraf/markup')
const WizardScene = require('telegraf/scenes/wizard')
const Composer = require('telegraf/composer')
const {randomInt} = require('../utils/helpers')

const bot = new Composer()

bot.action('accept', async ctx => {
	const state = ctx.scene.state

	try {
		await ctx.api.octoba.deleteItem(state.uri, state.pk)

		let gotoKeyboard = ctx.getKeyboard('resourceGoto', {
			modelName: state.modelName,
			pk: state.pk,
			excluded: ['octoba_model_goto_item']
		})

		ctx.editMessageText(ctx.i18n.t('octoba_model_deleted'), Extra.markdown().markup(m => {
			return m.inlineKeyboard(gotoKeyboard)
		}))

		return ctx.scene.leave()
	} catch (e) {
		console.log(e)
	}
})

bot.action(/cancel:(.*),(.*)/i, require('../handlers/item'))

/**
 * @param ctx Telegraf context
 * @param prepare Show prepare form text?
 * @param attributes Attributes in form
 * @param labels Labels for the attributes
 * @param type Type of form (store, update, updateOne)
 * @param ?id Id of item
 * @param ?uri URI
 * @param ?pk_name Name of primary key
 */
module.exports = new WizardScene('octoba-delete', async ctx => {
	const state = ctx.scene.state
	const accept = randomInt(0, 1)
	let keyboard = []
	let positions = [
		[Markup.callbackButton(ctx.i18n.t('octoba_messages_accept'), 'accept')],
		[Markup.callbackButton(ctx.i18n.t('octoba_messages_cancel'), `cancel:${state.modelName},${state.pk}`)]
	]

	for (let i = 0; i <= 1; i++) {
		if (accept) {
			keyboard[1] = positions[1]
			keyboard[0] = positions[0]
		} else {
			keyboard[1] = positions[0]
			keyboard[0] = positions[1]
		}
	}

	ctx.editMessageText(
		ctx.i18n.t('octoba_model_confirm_delete', {pk: state.pk}),
		Extra.markdown().markup(m => {
			return m.inlineKeyboard(keyboard)
		})
	)

	return ctx.wizard.next()
}, bot)
