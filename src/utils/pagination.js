const Markup = require('telegraf/markup');

module.exports = {
	simple: (paginator, params = {}) => {
		let items = [];
		paginator.data.map(item => {
			items.push([
				Markup.callbackButton(
					params.i18n.t(item[params.label_attribute]),
					`octoba.item:${params.modelName},${item[params.pk]}`
				)
			]);
		});

		// Prev
		if (paginator.page > 1) {
			items.push([
				Markup.callbackButton(
					params.i18n.t('octoba_messages_pager_prev'),
					`octoba.pager:${params.modelName},${paginator.page - 1}`
				)
			]);
		}

		// Next
		if (paginator.per_page * paginator.page <= paginator.total) {
			items.push([
				Markup.callbackButton(
					params.i18n.t('octoba_messages_pager_next'),
					`octoba.pager:${params.modelName},${paginator.page + 1}`
				)
			]);
		}

		// Main menu
		items.push([
			Markup.callbackButton(params.i18n.t('octoba_main'), 'octoba:main')
		]);

		return items;
	}
};
