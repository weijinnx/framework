const Markup = require('telegraf/markup');
const Composer = require('telegraf/composer');

const bot = new Composer();
bot.on('text', require('../forms/items/text'));
bot.on('audio', require('../forms/items/audio'));
bot.on('document', require('../forms/items/document'));
bot.on('photo', require('../forms/items/photo'));
bot.on('sticker', require('../forms/items/sticker'));
bot.on('video', require('../forms/items/video'));
bot.on('voice', require('../forms/items/voice'));
bot.on('contact', require('../forms/items/contact'));
bot.on('location', require('../forms/items/location'));

const validateInputType = (expectedType, inputType, i18n) => {
	const expectedTypes = {
		text: i18n.t('octoba_input_type_text'),
		audio: i18n.t('octoba_input_type_audio'),
		document: i18n.t('octoba_input_type_document'),
		photo: i18n.t('octoba_input_type_photo'),
		sticker: i18n.t('octoba_input_type_sticker'),
		video: i18n.t('octoba_input_type_video'),
		voice: i18n.t('octoba_input_type_voice'),
		contact: i18n.t('octoba_input_type_contact'),
		location: i18n.t('octoba_input_type_location'),
		callback_query: i18n.t('octoba_input_type_callback_query')
	};

	if (expectedType !== inputType) {
		return i18n.t('octoba_model_unexpected_type', {
			expected: expectedTypes[expectedType],
			sent: expectedTypes[inputType]
		});
	}

	return true;
};

module.exports = {
	validate: (state, validators, value, type = 'text', ctx) => {
		let errorMessage = null;

		const validatorsString = validators[state.form.attribute];

		if (!validatorsString) {
			return undefined;
		}

		const inputParams = validatorsString.split('@');
		const inputType = inputParams[0];
		const vInputType = validateInputType(inputType, type, ctx.i18n);

		if (vInputType !== true) {
			return vInputType;
		}

		const validatorsRules = inputParams[1].split('|');

		if (validatorsRules.length > 0) {
			validatorsRules.map(validator => {
				let vName = validator.split(':');
				if (Array.isArray(vName) && vName[0] !== '' && errorMessage === null || errorMessage === undefined) {
					errorMessage = ctx.config.octobaValidators[vName[0]](validator, value, ctx.i18n);
				}
			});
		}

		return errorMessage;
	},
	randomInt: (min, max) => {
		return Math.round(min - 0.5 + Math.random() * (max - min + 1));
	},
	getWizardFormOneSteps: (ctx, labels) => {
		let steps = [];
		steps.push(ctx => {
			ctx.reply(
				ctx.i18n.t('octoba_forms_label', {
					label: labels[ctx.scene.state.form.attribute]
				})
			);
			return ctx.wizard.next();
		});
		steps.push(bot);

		return steps;
	},
	getWizardFormManySteps: async (ctx, labels) => {
		let steps = [];
		let state = ctx.scene.state;

		await state.form.attributes.map(attribute => {
			const stepPush = (attribute) => {
				state.form.attribute = attribute;
				steps.push(ctx => {
					//ctx.scene.state.form.attribute = attribute;
					ctx.reply(
						ctx.i18n.t('octoba_forms_label', {
							label: labels[attribute]
						})
					);
					return ctx.wizard.next();
				});
				steps.push(bot);
			};


			return Promise.all([stepPush]).then(steps => {
				return steps;
			});
			// state.form.attribute = attribute;
			// steps.push(ctx => {
			// 	//ctx.scene.state.form.attribute = attribute;
			// 	ctx.reply(
			// 		ctx.i18n.t('octoba_forms_label', {
			// 			label: labels[attribute]
			// 		})
			// 	);
			// 	return ctx.wizard.next();
			// });
			// steps.push(bot);
		});

		console.log('Steps:', steps);

		return steps;
	},
	getParameteredURL: (url, params = {}) => {
		let paths = url.split('/');
		let match = [];
		let resultURL = url;

		if (paths.length > 0) {
			paths.map(key => {
				let matches = key.match(/(\{(.*)\})/);

				if (matches !== null) {
					match.push(matches[1]);
				} else {
					match.push(key);
				}
			});

			resultURL = match.join('/');
		}

		return resultURL;
	},
	defaultInlineGoto: (app, params = {}) => {
		const { i18n } = app;

		params.defaultButtons = {
			octoba_model_goto_list: `octoba.pager:${params.modelName},1`,
			octoba_model_goto_item: `octoba.item:${params.modelName},${params.pk}`,
			octoba_model_goto_main: 'octoba:main'
		};

		// if (params.pk === null) {
		// 	delete params.defaultButtons.octoba_model_goto_item
		// }

		let gotos = [],
			keyboard = [];

		Object.keys(params.defaultButtons).map(key => {
			gotos.push([
				key,
				[
					Markup.callbackButton(
						i18n.t(key),
						params.defaultButtons[key]
					)
				]
			]);
		});

		if (Array.isArray(params.excluded) && params.excluded.length > 0) {
			gotos.map(key => {
				params.excluded.map(ex => {
					if (ex !== key[0]) {
						keyboard.push(key[1]);
					}
				});
			});
		} else {
			gotos.map(key => {
				keyboard.push(key[1]);
			});
		}

		return keyboard;
	}
};
