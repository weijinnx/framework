module.exports = ctx => {
	let modelName = ctx.match[1]
	let pk = parseInt(ctx.match[2], 10)
	let model = ctx.getModel(modelName)

	return ctx.scene.enter('octoba-delete', {
		uri: model.crud.delete.uri,
		prepare: true,
		pk: pk,
		modelName: modelName
	})
}
