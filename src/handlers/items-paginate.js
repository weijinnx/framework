module.exports = ctx => {
	let modelName = ctx.match[1]
	let page = parseInt(ctx.match[2], 10)
	let params = ctx.config.paginator

	params.page = page

	return ctx.itemsWithPagination(ctx, modelName, params, true)
}
